use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    let option = 3;

    if option == 1 {
        guessing_game(); 
    } else if option == 2 {
        condition_playground();
    } else if option == 3 {
        loops_playground();
    }
    
}

fn strings() {
    let string_literal = "sadfasd"; // Immutable - known size at runtime - that's why it's faster, but only in that case

    let mut string = String::from("sdfsdf"); // Mutable
    string.push_str("string");
}

fn loops_playground() {
    let mut counter = 0;

    // LOOP
    // Returning value from a loop
    let result = 'loop_label: loop { // 'loop_label - single quote at the beginning of ethe label name - can be used with break and continue
        counter += 1; // increment

        if counter == 10 {
            break 'loop_label counter * 2; // Returning from here after 'break' - can be combined with a loop label
        }
    };

    println!("The result is {result}");

    // WHILE
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }

     // FOR
    for element in a {
        println!("the value is: {element}");
    }

    for number in (1..4).rev() { // Range
        println!("{number}!");
    }

}

fn condition_playground() {
    let number: u8 = 2;

    if number != 2 { // Types must be the same
        println!("number was something other than zero");
    }

    let condition = true;
    let number = if !condition { 2 } else if number == 2 {5} else { 3 }; // One liner - must be the same type

    println!("The value of number is: {number}");
}

fn guessing_game() {
    let mut guess_counter: i8 = 0;
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    // println!("The secret number is: {secret_number}");

    loop {
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(err) => {
                println!("Value: {guess}");
                println!("Error: {err}");
                continue;
            }
        };

        guess_counter = guess_counter + 1;

        println!("You guessed: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                println!("It took you {guess_counter} guesses!");
                break;
            }
        }
    }
}